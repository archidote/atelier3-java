# atelier3-java

## Démarrer l'application en développement

```
git clone git@gitlab.com:brlndtech/atelier3-java.git
cd atelier3-java/frontend/
npm install
cd ..
docker-compose up
```

## Démarrer l'application en production

```
git clone git@gitlab.com:brlndtech/atelier3-java.git
cd atelier3-java/frontend/
npm install
npm run build
cd ..
docker-compose -f docker-compose.prod.yml up
```

L'application est accessible via http://localhost/
