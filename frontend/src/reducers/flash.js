import { FLASH_DISPLAY, FLASH_RESET } from '../actions/actions'

const initialState = {
    init: false,
    items: null
}

const reducer = (state = initialState, { type, payload }) => {
    switch (type) {
        case FLASH_DISPLAY:
            return {
                init: true,
                items: payload
            }
        case FLASH_RESET:
            return initialState
        default:
            return state
    }
}

export default reducer;