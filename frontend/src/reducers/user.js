import { CREATE_USER, DELETE_USER } from '../actions/actions'

const initialState = {
    init: false,
    items: null
}

const reducer = (state = initialState, { type, payload }) => {
    switch (type) {
        case CREATE_USER:
            return {
                init: true,
                items: payload
            }
        case DELETE_USER:
            return initialState
        default:
            return state
    }
}

export default reducer;