import { combineReducers } from 'redux';
import { createTransform, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import storageSession from 'redux-persist/lib/storage/session';
import { parse, stringify } from 'flatted';

import card from './card';
import flash from './flash';
import game from './game';
import market from './market';
import user from './user';

const transformCircular = createTransform(
    (inboundState) => stringify(inboundState),
    (outboundState) => parse(outboundState),
);

const persistConfig = {
    key: 'root',
    storage: process.env.NODE_ENV === 'production' ? storage : storageSession,
    transforms: [transformCircular]
};

const rootReducer = combineReducers({
    card,
    flash,
    game,
    market,
    user
});

export default persistReducer(persistConfig, rootReducer);