import { CREATE_GAMES } from '../actions/actions'

const initialState = {
    init: false,
    items: null
}

const reducer = (state = initialState, { type, payload }) => {
    switch (type) {
        case CREATE_GAMES:
            return {
                init: true,
                items: payload
            }
        default:
            return state
    }
}

export default reducer;