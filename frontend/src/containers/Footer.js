import React from 'react';

import { Div } from '../components';

class Footer extends React.Component {
    render() {
        return (
            <Div b="0" bc="#404040" c="#cecece" h="50px" w="100%" fixed center middle>
                Atelier 3 - Landry COTET, Geoffrey SAUVAGEOT-BERLAND, Victor MAGAT
            </Div>
        )
    }
}

export default Footer;