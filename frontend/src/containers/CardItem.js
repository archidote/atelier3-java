import React from 'react';

import { Div } from '../components';

class CardItem extends React.Component {
    render() {
        const { card: { attack, defense, energy, hp, image, name } } = this.props;

        return (
            <Div vertical mr="10px">
                <Div c="#cecece" h="560px" w="300px" image={image} center p="10px" bold vertical>
                    <Div center>{name}</Div>
                    <Div mt="auto">
                        <Div w="calc(100% / 4)" center>{`❤️ ${hp}`}</Div>
                        <Div w="calc(100% / 4)" center>{`🗡 ${attack}`}</Div>
                        <Div w="calc(100% / 4)" center>{`🛡 ${defense}`}</Div>
                        <Div w="calc(100% / 4)" center>{`⚡ ${energy}`}</Div>
                    </Div>
                </Div>
            </Div>
        )
    }
}

export default CardItem;