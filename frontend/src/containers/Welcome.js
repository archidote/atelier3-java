import React from 'react';

import { Div } from '../components';

class Welcome extends React.Component {
    render() {
        return (
            <Div c="#cecece" w="100%" center middle>
                Connectez vous
            </Div>
        )
    }
}

export default Welcome;