import React from 'react';

import { Button, Div } from '../components';

class CombatItem extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            collapsed: true
        }

        this.handleClick = this.handleClick.bind(this);
    }

    handleClick() {
        const { collapsed } = this.state;

        this.setState({
            collapsed: !collapsed
        });
    }

    render() {
        const { game, user } = this.props;
        const { collapsed } = this.state;

        return (
            <Div w="100%" vertical middle mr="10px">
                <Div w="100%" middle center>
                    <Div mr="10px">{`${game.winner === user.id ? 'Victoire' : 'Défaite'} en ${game.logs.length} rounds le ${game.gameDate}`}</Div>
                    <Button label={collapsed ? "➕" : "➖"} w="fit-content" onClick={this.handleClick} />
                </Div>
                {!collapsed && game.logs.map(log => (
                    <Div key={log}>{log}</Div>
                ))}
            </Div>
        )
    }
}

export default CombatItem;