import React from 'react';
import { connect } from 'react-redux'

import { flashDisplay } from '../actions';
import { createUser } from '../api';
import { Button, Div, Input } from '../components';

class Inscription extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            disabled: false,
            label: "S'enregistrer",
            username: '',
            password: '',
            nom: '',
            prenom: ''
        }

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(e) {
        let input = {};
        input[e.target.placeholder.toLowerCase()] = e.target.value;
        this.setState(input);
    }

    handleSubmit() {      
        const { dispatchFlashDisplay } = this.props;  
        const { username, password, nom, prenom } = this.state;
        this.setState({
            disabled: true,
            label: 'Chargement ...'
        })

        createUser({ username, password, nom, prenom }, (data) => {
            if (data === 'Error') {
                dispatchFlashDisplay({ msg: `Username déjà enregistré, choisissez en un autre !`, success: false })
            } else {
                dispatchFlashDisplay({ msg: `Vous êtes inscrit en tant que ${data.username}. Connectez vous maintenant !`, success: true })
            }
            this.setState({
                disabled: false,
                label: "S'enregistrer"
            })
        });
    }

    render() {
        const { disabled, label, username, password, nom, prenom } = this.state;
        return (
            <Div c="#cecece" w="100%" center middle vertical>
                <Input type="text" placeholder="Username" onChange={this.handleChange} value={username} />
                <Input type="password" placeholder="Password" onChange={this.handleChange} value={password} />
                <Input type="text" placeholder="Nom" onChange={this.handleChange} value={nom} />
                <Input type="text" placeholder="Prenom" onChange={this.handleChange} value={prenom} />
                <Button disabled={disabled} label={label} onClick={this.handleSubmit} />
            </Div>
        )
    }
}

const mapDispatchToProps = {
    dispatchFlashDisplay: flashDisplay,
}

export default connect(null, mapDispatchToProps)(Inscription);