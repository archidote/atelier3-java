import React from 'react';
import { connect } from 'react-redux';

import { getGames } from '../api';
import { createGames } from '../actions';
import { Div } from '../components';
import { CombatItem } from '.';

class CombatList extends React.Component {
    constructor(props) {
        super(props);

        this.refreshCombats = this.refreshCombats.bind(this);
    }

    componentDidMount() {
        this.refreshCombats();
    }

    refreshCombats() {
        const { dispatchCreateGames, initUser, itemsUser } = this.props;

        if (initUser) {
            getGames(itemsUser.id, (data) => {
                dispatchCreateGames(data);
            });
        }
    }
    
    render() {
        const { init, items, itemsUser } = this.props;

        if (!init) {
            return (
                <Div c="#cecece" w="100%" center middle>
                    Chargement ...
                </Div>
            )
        }

        if (items && items.length === 0) {
            return (
                <Div c="#cecece" w="100%" center middle>
                    Vous n'avez aucun combats
                </Div>
            )
        }

        return (
            <Div c="#cecece" w="100%" center middle vertical>
                {items.map(game => (
                    <CombatItem key={game.id} game={game} user={itemsUser} />
                ))}
            </Div>
        )
    }
}

const mapStateToProps = ({ game: { init, items }, user: { init: initUser, items: itemsUser} }) => ({
    init,
    initUser,
    items,
    itemsUser,
});

const mapDispatchToProps = {
    dispatchCreateGames: createGames,
};

export default connect(mapStateToProps, mapDispatchToProps)(CombatList);