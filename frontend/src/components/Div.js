import React from 'react';
import styled from 'styled-components';

const Div = styled.div`
    display: flex;
    flex-direction: ${({ vertical }) => vertical ? 'column' : 'row'};
    ${({ b }) => b && `bottom: ${b};`}
    ${({ t }) => t && `top: ${t};`}
    ${({ bc }) => bc && `background-color: ${bc};`}
    ${({ w }) => w && `width: ${w};`}
    ${({ c }) => c && `color: ${c};`}
    ${({ h }) => h && `height: ${h};`}
    font-size: 18px;
    ${({ shadow }) => shadow && 'box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.3), 0 6px 20px 0 rgba(0, 0, 0, 0.3);'}
    ${({ fixed }) => fixed && 'position: fixed;'}
    ${({ center }) => center && 'justify-content: center;'}
    ${({ right }) => right && 'justify-content: flex-end;'}
    ${({ middle }) => middle && 'align-items: center;'}
    ${({ borderBottomHover }) => borderBottomHover && `
        border-bottom: solid 3px transparent;
        margin-top: 3px;
        transition: 0.4s;

        :hover {
            border-bottom: solid 3px #cecece;
            cursor: pointer;
        }
    `}
    ${({ borderBottom }) => borderBottom && `
        border-bottom: solid 3px #cecece;
        margin-top: 3px;
    ` }
    ${({ mt }) => mt && `margin-top: ${mt};`}
    ${({ mr }) => mr && `margin-right: ${mr};`}
    ${({ p }) => p && `padding: ${p};`}
    ${({ image }) => image && `background-image: url(${image});`}
    ${({ bold }) => bold && `font-weight: bold;`}
`;

const StyledDiv = ({ b, bc, bold, borderBottom, borderBottomHover, c, center, fixed, h, image, middle, mt, mr, onClick, p, right, shadow, t, vertical, w, ...otherProps }) =>
    <Div 
        b={b} bc={bc} bold={bold} borderBottom={borderBottom} borderBottomHover={borderBottomHover}
        c={c} center={center} fixed={fixed} h={h} image={image} middle={middle} mt={mt}
        mr={mr} onClick={onClick} p={p} right={right} shadow={shadow}
        t={t} vertical={vertical} w={w}
    >{otherProps.children}</Div>

export default StyledDiv;