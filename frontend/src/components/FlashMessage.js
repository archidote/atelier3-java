import React from 'react';
import styled, { keyframes } from 'styled-components';
import { fadeInLeft, fadeOutLeft } from 'react-animations';
 
const fadeInLeftAnimation = keyframes`${fadeInLeft}`;
const fadeOutLeftAnimation = keyframes`${fadeOutLeft}`;
let storedSuccess = null;

const P = styled.p`
    position: fixed;
    top: 50px;
    display: flex;
    width: 100%;
    margin: 0;
    height: 50px;
    align-items: center;
    color: #cecece;
    animation: ${({ animateIn }) => animateIn ? fadeInLeftAnimation : fadeOutLeftAnimation} 0.3s linear;
    visibility: ${({ visible }) => visible ? 'visible' : 'hidden'};
    font-weight: bold;
    justify-content: center;
    font-size: 18px;
    ${({ animateIn }) => animateIn && 'transition: 0.4s;'}
    ${({ success }) => `
        background-color: ${success ? '#548a42' : '#7a3535'};
        color: ${success ? '#36592b' : '#522525'};
    
        :hover {
            background-color: ${success ? '#36592b' : '#522525'};
            color: ${success ? '#548a42' : '#7a3535'};
            cursor: pointer;
        }
    `}
`;

const StyledDiv = ({ animateIn, msg, onClick, success, visible }) => {
    if (success !== null) {
        storedSuccess = success;
    }
    return <P animateIn={animateIn} onClick={onClick} success={success === null ? storedSuccess : success} visible={visible}>{msg}</P>
}

export default StyledDiv;