import { get, post } from './methods';

export const subscribe = (cardId, bet, callback) => post("game/subscribe/" + cardId + "/bet/" + bet, "", callback);
export const unsubscribe = (cardId, callback) => post("game/unsubscribe/" + cardId, "", callback);
export const getGames = (userId, callback) => get("game/user/" + userId, callback);