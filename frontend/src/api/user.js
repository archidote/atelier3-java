import { get, post } from './methods'

export const getUsers = callback => get("user", callback);
export const createUser = (user, callback) => post("user/create", user, callback);
export const getUser = (userId, callback) => get("user/" + userId, callback);