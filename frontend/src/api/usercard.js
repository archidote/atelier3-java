import { get } from './methods'

export const getCards = (id, callback) => get("usercard/user/" + id, callback);