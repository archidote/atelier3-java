import { post } from './methods'

export const cancelSell = (card, callback) => post("market/cancelSell", card, callback);
export const sell = (price, card, callback) => post("market/sell/" + price, card, callback);
export const buy = (cardId, user, callback) => post("market/buy/" + cardId, user, callback);