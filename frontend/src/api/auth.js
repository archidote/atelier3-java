import { post } from './methods';

export const connect = (user, callback) => post("auth/connection", user, callback);