import { FLASH_RESET, FLASH_DISPLAY } from './actions'

export const flashReset = () => ({
    type: FLASH_RESET,
    payload: {}
})

export const flashDisplay = flash => ({
    type: FLASH_DISPLAY,
    payload: flash
})