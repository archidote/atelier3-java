import { CREATE_GAMES } from './actions'

export const createGames = (games) => ({
    type: CREATE_GAMES,
    payload: games,
});