import { CREATE_CARDS } from './actions'

export const createCards = (cards) => ({
    type: CREATE_CARDS,
    payload: cards,
});