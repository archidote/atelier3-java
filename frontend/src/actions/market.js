import { CREATE_MARKET_CARDS } from './actions'

export const createMarketCards = (cards) => ({
    type: CREATE_MARKET_CARDS,
    payload: cards,
});