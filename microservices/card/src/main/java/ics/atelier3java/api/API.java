package ics.atelier3java.api;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.Map.Entry;
import java.util.Random;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

public class API {
	
	private static API instance = null;
	private List<String> cardNames;
	
	private API() {
		try {
			URL url = new URL("http://ddragon.leagueoflegends.com/cdn/11.7.1/data/fr_FR/champion.json");
			HttpURLConnection httpRequest = (HttpURLConnection) url.openConnection();
			httpRequest.setRequestMethod("GET");
			BufferedReader in = new BufferedReader(
			  new InputStreamReader(httpRequest.getInputStream()));
			String inputLine;
			StringBuffer content = new StringBuffer();
			while ((inputLine = in.readLine()) != null) {
			    content.append(inputLine);
			}
			in.close();
			
			ObjectMapper objectMapper = new ObjectMapper();
			objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			DataAPI<Object> cards = objectMapper.readValue(content.toString(), new TypeReference<DataAPI<Object>>(){});
			this.cardNames = Arrays.asList(cards.getData().keySet().toArray(new String[0]));
		} catch(Exception e) {
			System.out.println(e);
		}
	}
	
	public DataAPICard getRandomCard() {
		DataAPICard retour = null;
		try {
			String cardName = cardNames.get(new Random().nextInt(cardNames.size()));
			URL url = new URL("http://ddragon.leagueoflegends.com/cdn/11.7.1/data/fr_FR/champion/" + cardName + ".json");
			HttpURLConnection httpRequest = (HttpURLConnection) url.openConnection();
			httpRequest.setRequestMethod("GET");
			BufferedReader in = new BufferedReader(
			  new InputStreamReader(httpRequest.getInputStream()));
			String inputLine;
			StringBuffer content = new StringBuffer();
			while ((inputLine = in.readLine()) != null) {
			    content.append(inputLine);
			}
			in.close();
			
			ObjectMapper objectMapper = new ObjectMapper();
			objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			DataAPI<DataAPICard> cards = objectMapper.readValue(content.toString(), new TypeReference<DataAPI<DataAPICard>>(){});
			for (Entry<String, DataAPICard> card : cards.getData().entrySet()) {
				retour = card.getValue();
			}
		} catch (Exception e) {
			System.out.println(e);
		}
		return retour;
	}
	
	public static API getInstance() {
		if (instance == null) {
			instance = new API();
		}
		return instance;
	}
}
