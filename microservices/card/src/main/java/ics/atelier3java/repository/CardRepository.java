package ics.atelier3java.repository;

import org.springframework.data.repository.CrudRepository;

import ics.atelier3java.model.Card;


public interface CardRepository extends CrudRepository<Card, Integer> {
			
}




