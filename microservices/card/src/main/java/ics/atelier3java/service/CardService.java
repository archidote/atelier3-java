package ics.atelier3java.service;

import java.util.List;
import java.util.Optional;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import ics.atelier3java.common.dto.CardDTO;
import ics.atelier3java.common.dto.UserDTO;
import ics.atelier3java.model.Card;
import ics.atelier3java.repository.CardRepository;

@RestController
public class CardService {
	
	@Autowired
	private CardRepository cardRepository;
	
	public Card createCard(Card card) {
		return cardRepository.save(card);
	}
	
	public List<Card> getCards() {
		List<Card> cardList = new ArrayList<Card>();
		cardRepository.findAll().forEach(cardList::add);
		return cardList;
	}

	public List<Card> generateRandomCards(int nb) {
		List<Card> cardList = new ArrayList<Card>();
		for (int i = 0; i < nb; i++) {
			Card card = Card.generateCard();
			createCard(card);
			cardList.add(card);
		}
		return cardList;
	}
	
	public Card getById(int id) {
		Optional<Card> oCard = cardRepository.findById(id);
		if (oCard.isPresent()) {
			return oCard.get();
		} else {
			return null;
		}
	}
	
	public Card updateById(Card card) {
		return cardRepository.save(card);
	}
	
	public List<CardDTO> getCardsSelling() {
		List<CardDTO> cardList = new ArrayList<CardDTO>();
		Iterable<Card> iterableCard = cardRepository.findAll();
		for (Card card : iterableCard) {
			if (card.isSelling()) {
				RestTemplate restTemplate = new RestTemplate();
				ResponseEntity<String> response = restTemplate.getForEntity("http://usercard:8080/card/" + card.getId(), String.class);
				ObjectMapper objectMapper = new ObjectMapper();
				UserDTO user = null;
				try {
					user = objectMapper.readValue(response.getBody(), new TypeReference<UserDTO>(){});
				} catch (Exception e) {
					e.printStackTrace();
				}
				CardDTO cardDTO = card.toDTO();
				cardDTO.setUser(user);
				cardList.add(cardDTO);
			}
		}
		return cardList;
	}
}
