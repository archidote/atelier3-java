package ics.atelier3java.repository;

import org.springframework.data.repository.CrudRepository;

import ics.atelier3java.model.User;

import java.util.Optional;

public interface UserRepository extends CrudRepository<User, Integer> {
	
	Optional<User> findByUsername(String username);
	
}
