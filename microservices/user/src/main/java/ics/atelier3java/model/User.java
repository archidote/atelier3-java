package ics.atelier3java.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import ics.atelier3java.common.dto.UserDTO;

@Entity
@Table(name = "User", uniqueConstraints={ @UniqueConstraint(columnNames = { "username" }) })
public class User {
	
	@Id
	@GeneratedValue
	private int id;
	private String username;
	private String password;
	private String nom;
	private String prenom;
	private int age;
	private int money;
	
	public User() {}
	
	public User(int id, String username, String password, String nom, String prenom, int age, int money) {
		super();
		this.id = id;
		this.username = username;
		this.password = password;
		this.nom = nom;
		this.prenom = prenom;
		this.age = age;
		this.money = money;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public int getMoney() {
		return money;
	}

	public void setMoney(int money) {
		this.money = money;
	}
	
	public UserDTO toDTO() {
		return new UserDTO(this.id, this.username, this.password, this.nom, this.prenom, this.age, this.money);
	}

}
