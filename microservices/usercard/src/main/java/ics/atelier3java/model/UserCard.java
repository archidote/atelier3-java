package ics.atelier3java.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "UserCard")
public class UserCard {
	
	@Id
	@GeneratedValue
	private int id;
	private int userId;
	private int cardId;
	
	public UserCard() {
		super();
	}
	
	public UserCard(int userId, int cardId) {
		super();
		this.userId = userId;
		this.cardId = cardId;
	}
	
	public UserCard(int id, int userId, int cardId) {
		super();
		this.id = id;
		this.userId = userId;
		this.cardId = cardId;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getCardId() {
		return cardId;
	}
	public void setCardId(int cardId) {
		this.cardId = cardId;
	}

}
