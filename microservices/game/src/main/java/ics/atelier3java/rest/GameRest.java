package ics.atelier3java.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ics.atelier3java.model.Game;
import ics.atelier3java.service.GameService;

@RestController
@RequestMapping()
public class GameRest {
	
	@Autowired
	private GameService gameService;
	
	@GetMapping("/list")
	public List<Game> getGames() {
		return gameService.getGames();
	}
	
	@GetMapping("/{id}")
	public Game getGameById(@PathVariable int id) {
		return gameService.getGameById(id);
	}

	@GetMapping("/user/{userId}")
	public List<Game> getGamesByUserId(@PathVariable int userId) {
		return gameService.getGamesByUserId(userId);
	}
}
