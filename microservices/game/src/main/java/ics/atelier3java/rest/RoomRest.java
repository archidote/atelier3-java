package ics.atelier3java.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ics.atelier3java.service.RoomService;

@RestController
@RequestMapping()
public class RoomRest {
	
	@Autowired
	private RoomService roomService;
	
	@PostMapping("/subscribe/{cardId}/bet/{bet}")
	public boolean subscribe(@PathVariable int cardId, @PathVariable int bet) {
		return roomService.subscribe(cardId, bet);
	}
	
	@PostMapping("/unsubscribe/{cardId}")
	public boolean unsubscribe(@PathVariable int cardId) {
		return roomService.unsubscribe(cardId);
	}
}
