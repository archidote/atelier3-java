package ics.atelier3java.model;

import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import ics.atelier3java.common.dto.CardDTO;

@Entity
@Table(name = "Game")
public class Game {
	
	@Id
	@GeneratedValue
	private int id;
	private String gameDate;
	private Integer userIdOne = null;
	private Integer userIdTwo = null;
	private int winner;
	@Column( length = 100000 )
	private ArrayList<String> logs = new ArrayList<String>();
	
	/**
	 * Unstored field
	 */
	@Transient
	private CardDTO cardOne;
	@Transient
	private CardDTO cardTwo;
	@Transient
	private int criticalTwo = 30;
	@Transient
	private int criticalFive = 15;
	@Transient
	private int min = 1;
	@Transient
	private int max = 100;
	@Transient
	private CardDTO cardTurn;
	@Transient
	private int compteur = 0;
	
	public Game() {}
	
	public Game(CardDTO cardOne, CardDTO cardTwo, String gameDate) {
		super();
		this.cardOne = cardOne;
		this.cardTwo = cardTwo;
		this.gameDate = gameDate;
		this.userIdOne = cardOne.getUser().getId();
		this.userIdTwo = cardTwo.getUser().getId();
	}
	
	public int getId() {
		return id;
	}

	public CardDTO getCardOne() {
		return cardOne;
	}

	public void setCardOne(CardDTO cardOne) {
		this.cardOne = cardOne;
	}

	public CardDTO getCardTwo() {
		return cardTwo;
	}

	public void setCardTwo(CardDTO cardTwo) {
		this.cardTwo = cardTwo;
	}

	public String getGameDate() {
		return gameDate;
	}

	public void setGameDate(String gameDate) {
		this.gameDate = gameDate;
	}

	public int getWinner() {
		return winner;
	}

	public void setWinner(int winner) {
		this.winner = winner;
	}
	
	public ArrayList<String> getLogs() {
		return this.logs;
	}
	
	private CardDTO getFromCardTurn(boolean opposite) {
		if (cardTurn.getId() == cardOne.getId()) {
			return opposite ? cardOne : cardTwo;
		} else {
			return opposite ? cardTwo : cardOne;
		}
	}
	
	private void round(boolean changeCardTurn) {
		compteur++;
		String log = "Round n°" + compteur + " : ";
		if (changeCardTurn) {
			cardTurn = getFromCardTurn(false);
		}
		log += cardTurn.getName() + " inflige ";
		int attack = cardTurn.getAttack();
		int random = (int) Math.floor(Math.random()*(max-min+1)+min);
		if (random <= criticalTwo) {
			attack = attack * 2;
		}
		random = (int) Math.floor(Math.random()*(max-min+1)+min);
		if (random <= criticalFive) {
			attack = attack * 5;
		}
		CardDTO cardDefense = getFromCardTurn(false);
		attack = attack - cardDefense.getDefense();
		log += attack + " de dégats à " + cardDefense.getName() + ". ";
		if (attack < 0) {
			cardTurn.setHp(cardTurn.getHp() + attack);
			log += cardTurn.getName() + " perd " + (attack * -1) + " HP.";
		} else {
			cardDefense.setHp(cardDefense.getHp() - attack);
			log += cardDefense.getName() + " perd " + attack + " HP. ";
		}
		log += cardTurn.getName() + " (" + cardTurn.getHp() + " HP) et " + cardDefense.getName() + " (" + cardDefense.getHp() + " HP).";
		logs.add(log);
	}
	
	public void fight() {
		System.out.println("userIdOne = " + userIdOne);
		System.out.println("userIdTwo = " + userIdTwo);
		if (userIdOne != null && userIdTwo != null) {
			int random = (int) Math.floor(Math.random()*(max-min+1)+min);
			cardTurn = cardOne;
			if (random % 2 == 0) {
				cardTurn = cardTwo;
			}
			round(false);
			while (cardOne.getHp() > 0 && cardTwo.getHp() > 0) {
				round(true);
			}
			winner = cardOne.getHp() <= 0 ? cardTwo.getUser().getId() : cardOne.getUser().getId();
		}
	}

	public Integer getUserIdOne() {
		return userIdOne;
	}

	public void setUserIdOne(Integer userIdOne) {
		this.userIdOne = userIdOne;
	}

	public Integer getUserIdTwo() {
		return userIdTwo;
	}

	public void setUserIdTwo(Integer userIdTwo) {
		this.userIdTwo = userIdTwo;
	}
	
}
