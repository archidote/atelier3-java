package ics.atelier3java.service;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import ics.atelier3java.common.dto.CardDTO;
import ics.atelier3java.common.dto.UserDTO;

@RestController
public class RoomService {
	
	private Map<Integer, Entry<CardDTO, Integer>> cards = new HashMap<Integer, Entry<CardDTO, Integer>>();
			
	@Autowired
	private GameService gameService;
	
	private CardDTO getCard(int cardId) {
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<String> response = restTemplate.getForEntity("http://card:8080/" + cardId, String.class);
		ObjectMapper objectMapper = new ObjectMapper();
		CardDTO card = null;
		try {
			card = objectMapper.readValue(response.getBody(), new TypeReference<CardDTO>(){});
			if (card != null) {
				response = restTemplate.getForEntity("http://usercard:8080/card/" + cardId, String.class);
				UserDTO user = objectMapper.readValue(response.getBody(), new TypeReference<UserDTO>(){});
				if (user != null) {
					card.setUser(user);
					return card;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
		
	public boolean subscribe(int cardId, int bet) {
		CardDTO card = getCard(cardId);
		if (card != null && card.getEnergy() >= 20 && !cards.containsKey(card.getId()) && card.getUser().getMoney() >= bet && !card.isSelling()) {
			Entry<CardDTO, Integer> opponent = null;
			for (Entry<Integer, Entry<CardDTO, Integer>> cardInList : cards.entrySet()) {
				if (cardInList.getValue().getKey().getUser().getId() != card.getUser().getId()) {
					opponent = cardInList.getValue();
					break;
				}
			}
			if (opponent != null) {
				cards.remove(opponent.getKey().getId());
				gameService.fight(opponent, Map.entry(card, bet));
			} else {
				cards.put(card.getId(), Map.entry(card, bet));
				card.setInRoom(true);
				RestTemplate restTemplate = new RestTemplate();
				ObjectMapper mapper = new ObjectMapper();
				String jsonInString = "";
				try {
					jsonInString = mapper.writeValueAsString(card);
				} catch (JsonProcessingException e) {
					e.printStackTrace();
				}
				HttpHeaders headers = new HttpHeaders();
			    headers.setContentType(MediaType.APPLICATION_JSON);
				HttpEntity<String> request = new HttpEntity<String>(jsonInString, headers);
				restTemplate.put("http://card:8080/" + card.getId(), request);
			}
			return true;
		}
		return false;
	}
	
	public boolean unsubscribe(int cardId) {
		CardDTO card = getCard(cardId);
		if (card != null) {
			cards.remove(card.getId());
			card.setInRoom(false);
			RestTemplate restTemplate = new RestTemplate();
			ObjectMapper mapper = new ObjectMapper();
			String jsonInString = "";
			try {
				jsonInString = mapper.writeValueAsString(card);
			} catch (JsonProcessingException e) {
				e.printStackTrace();
			}
			HttpHeaders headers = new HttpHeaders();
		    headers.setContentType(MediaType.APPLICATION_JSON);
			HttpEntity<String> request = new HttpEntity<String>(jsonInString, headers);
			restTemplate.put("http://card:8080/" + card.getId(), request);
			return true;
		}
		return false;
	}
	
	public void getEnergyBack(CardDTO card) {
		Thread t = new Thread() {
			public void run() {
				while (card.getEnergy() < 100) {
					try {
						card.setEnergy(card.getEnergy() + 1);
						System.out.println("Regain d'énergie de la carte " + card.getId());
						RestTemplate restTemplate = new RestTemplate();
						ObjectMapper mapper = new ObjectMapper();
						String jsonInString = "";
						try {
							jsonInString = mapper.writeValueAsString(card);
						} catch (JsonProcessingException e) {
							e.printStackTrace();
						}
						HttpHeaders headers = new HttpHeaders();
					    headers.setContentType(MediaType.APPLICATION_JSON);
						HttpEntity<String> request = new HttpEntity<String>(jsonInString, headers);
						restTemplate.put("http://card:8080/" + card.getId(), request);
						Thread.sleep(300000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		};
	    t.start();
	}

}
