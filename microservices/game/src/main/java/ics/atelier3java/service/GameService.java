package ics.atelier3java.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.TimeZone;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ics.atelier3java.common.dto.CardDTO;
import ics.atelier3java.common.dto.UserDTO;
import ics.atelier3java.model.Game;
import ics.atelier3java.repository.GameRepository;

@RestController
public class GameService {
	
	@Autowired
	private GameRepository gameRepository;
	
	@Autowired
	private RoomService roomService;
	
	public List<Game> getGames() {
		List<Game> gameList = new ArrayList<Game>();
		gameRepository.findAll().forEach(gameList::add);
		return gameList;
	}
	
	public Game getGameById(int id) {
		Optional<Game> oGame = gameRepository.findById(id);
		if (oGame.isPresent()) {
			return oGame.get();
		} else {
			return null;
		}
	}

	public List<Game> getGamesByUserId(int userId) {
		List<Game> gamesByUser = new ArrayList<Game>();
		for (Game game : getGames()) {
			if (game.getUserIdOne() == userId || game.getUserIdTwo() == userId) {
				gamesByUser.add(game);
			}
		}
		return gamesByUser;
	}
	
	public void fight(Entry<CardDTO, Integer> entryCardOne, Entry<CardDTO, Integer> entryCardTwo) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	    simpleDateFormat.setTimeZone(TimeZone.getTimeZone("Europe/Paris"));
	    CardDTO cardOne = entryCardOne.getKey();
	    CardDTO cardTwo = entryCardTwo.getKey();
	    UserDTO userOne = cardOne.getUser();
		UserDTO userTwo = cardTwo.getUser();
		Game game = new Game(
				new CardDTO(cardOne.getId(), cardOne.isSelling(), cardOne.getPrice(), cardOne.getName(), cardOne.getDescription(), cardOne.getAttack(), cardOne.getDefense(), cardOne.getHp(), cardOne.getImage(), cardOne.isInRoom(), cardOne.getEnergy(), cardOne.getUser()),
				new CardDTO(cardTwo.getId(), cardTwo.isSelling(), cardTwo.getPrice(), cardTwo.getName(), cardTwo.getDescription(), cardTwo.getAttack(), cardTwo.getDefense(), cardTwo.getHp(), cardTwo.getImage(), cardTwo.isInRoom(), cardTwo.getEnergy(), cardTwo.getUser()),
				simpleDateFormat.format(new Date()).toString());
		game.fight();
		gameRepository.save(game);
		cardOne.setInRoom(false);
		cardTwo.setInRoom(false);
		cardOne.setEnergy(userOne.getId() == game.getWinner() ? cardOne.getEnergy() - 10 : cardOne.getEnergy() - 20);
		cardTwo.setEnergy(userTwo.getId() == game.getWinner() ? cardTwo.getEnergy() - 10 : cardTwo.getEnergy() - 20);
		roomService.getEnergyBack(cardOne);
		roomService.getEnergyBack(cardTwo);
		userOne.setMoney(userOne.getId() == game.getWinner() ? userOne.getMoney() + entryCardOne.getValue() : userOne.getMoney() - entryCardOne.getValue());
		userTwo.setMoney(userTwo.getId() == game.getWinner() ? userTwo.getMoney() + entryCardTwo.getValue() : userTwo.getMoney() - entryCardTwo.getValue());
		RestTemplate restTemplate = new RestTemplate();
		ObjectMapper mapper = new ObjectMapper();
		String jsonInString = "";
		try {
			jsonInString = mapper.writeValueAsString(cardOne);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		HttpHeaders headers = new HttpHeaders();
	    headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> request = new HttpEntity<String>(jsonInString, headers);
		restTemplate.put("http://card:8080/" + cardOne.getId(), request);
		try {
			jsonInString = mapper.writeValueAsString(cardTwo);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		request = new HttpEntity<String>(jsonInString, headers);
		restTemplate.put("http://card:8080/" + cardTwo.getId(), request);
		try {
			jsonInString = mapper.writeValueAsString(userOne);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		request = new HttpEntity<String>(jsonInString, headers);
		restTemplate.put("http://user:8080/" + userOne.getId(), request);
		try {
			jsonInString = mapper.writeValueAsString(userTwo);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		request = new HttpEntity<String>(jsonInString, headers);
		restTemplate.put("http://user:8080/" + userTwo.getId(), request);
	}
}
