package ics.atelier3java.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ics.atelier3java.common.dto.UserDTO;
import ics.atelier3java.service.AuthService;

@RestController
@RequestMapping()
public class AuthRest {
	
	@Autowired
	private AuthService authService;
	
	@PostMapping("/connection")
	public UserDTO connection(@RequestBody UserDTO user) {
		return authService.connection(user);
	}

}
