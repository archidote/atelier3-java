package ics.atelier3java.common.dto;

public class CardDTO {
	public CardDTO(int id, boolean selling, int price, String name, String description, int attack, int defense, int hp,
			String image, boolean inRoom, int energy, UserDTO user) {
		super();
		this.id = id;
		this.selling = selling;
		this.price = price;
		this.name = name;
		this.description = description;
		this.attack = attack;
		this.defense = defense;
		this.hp = hp;
		this.image = image;
		this.inRoom = inRoom;
		this.energy = energy;
		this.user = user;
	}
	public CardDTO() {
		super();
	}
	private int id;
	private boolean selling;	
	private int price;
	private String name;
	private String description;
	private int attack;
	private int defense;
	private int hp;
	private String image;
	private boolean inRoom;
	public int energy;
	private UserDTO user;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public boolean isSelling() {
		return selling;
	}
	public void setSelling(boolean selling) {
		this.selling = selling;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getAttack() {
		return attack;
	}
	public void setAttack(int attack) {
		this.attack = attack;
	}
	public int getDefense() {
		return defense;
	}
	public void setDefense(int defense) {
		this.defense = defense;
	}
	public int getHp() {
		return hp;
	}
	public void setHp(int hp) {
		this.hp = hp;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public boolean isInRoom() {
		return inRoom;
	}
	public void setInRoom(boolean inRoom) {
		this.inRoom = inRoom;
	}
	public int getEnergy() {
		return energy;
	}
	public void setEnergy(int energy) {
		this.energy = energy;
	}
	public UserDTO getUser() {
		return user;
	}
	public void setUser(UserDTO user) {
		this.user = user;
	}
}
